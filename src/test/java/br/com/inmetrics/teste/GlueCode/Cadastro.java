package br.com.inmetrics.teste.GlueCode;

import br.com.inmetrics.teste.pages.CadastrarFormPage;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class Cadastro {

    CadastrarFormPage passos;

    @Before
    public void init() {

        passos = new CadastrarFormPage();
    }

    @Dado("que eu acesso o site {string}")
    public void que_eu_acesso_o_site(String site) {
        passos.OpenSite(site);
    }

    @Entao("clicar no botao de cadastro")
    public void clicar_no_botao_de_cadastro() {
        passos.clicarCadastre();

    }

    @Quando("eu insiro o usuario {string} e a senha {string}")
    public void eu_insiro_o_usuario_e_a_senha(String usuario, String senha) {
        passos.digitarLogin(usuario);
        passos.digitarSenha(senha);
        passos.confirmarSenha(senha);
    }

    @Entao("eu clico no botão cadastrar")
    public void eu_clico_no_botão_cadastrar() {

        passos.clicarCadastrar();
    }

    @Entao("valido que usuario foi cadastrado com sucesso")
    public void valido_que_usuario_foi_cadastrado_com_sucesso() {
        String proximaPagina = passos.loginValidacao();

        assertEquals("Login", proximaPagina, "Valores  não conferem");
       // ScreenShot.tirarPrint(driver);
    }



    }

