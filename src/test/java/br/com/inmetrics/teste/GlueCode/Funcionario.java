package br.com.inmetrics.teste.GlueCode;

import br.com.inmetrics.teste.pages.FuncionarioPage;
import br.com.inmetrics.teste.support.ScreenShot;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class Funcionario {
    private static WebDriver driver;
    private WebElement element;
    FuncionarioPage passos;

    @Before
    public void init() {
        passos = new FuncionarioPage();
    }

    @Entao("eu clico no botão novo funcionario")
    public void eu_clico_no_botão_novo_funcionario() {
        passos.clicarFuncionario();


    }


    @Quando("exibida insiro Nome {string}")
    public void exibida_insiro_nome(String nome) {
        passos.escreverNome(nome);
    }

    @Quando("Cpf {string}")
    public void cpf(String cpf) {
        passos.escreverCpf(cpf);

    }

    @Quando("Sexo {string}")
    public void sexo(String sexo) {
        passos.escreverSexo(sexo);

    }

    @Quando("Data de admisao {string}")
    public void data_de_admisao(String data_de_admisao) {
        passos.escreverDataDeAdimicao(data_de_admisao);

    }

    @Quando("Cargo {string}")
    public void cargo(String cargo) {
        passos.escreverCargo(cargo);

    }

    @Quando("Salario {string}")
    public void salario(String salario) {
        passos.escreverSalario(salario);

    }

    @Quando("Tipo de contratação clt")
    public void tipo_de_contratação_clt() {
        passos.tipoDeContratacao();

    }

    @Quando("Clica em enviar")
    public void clica_em_enviar() {
        passos.clicarEnviar();
    }

    @Quando("Validar que o funcionario foi criado com sucesso")
    public void validar_que_o_funcionario_foi_criado_com_sucesso() {
        String criadoComSucesso = passos.validarFuncionarioCriadocomSucesso();
        assertEquals("SUCESSO! Usuário cadastrado com sucesso\n" +
                "×", criadoComSucesso, "Valores  não conferem");

        ScreenShot.tirarPrint(driver);

    }


}
