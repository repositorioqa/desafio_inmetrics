package br.com.inmetrics.teste.GlueCode;

import br.com.inmetrics.teste.pages.LoginPage;
import br.com.inmetrics.teste.support.ScreenShot;
import io.cucumber.java.Before;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class Login {
    private static WebDriver driver;
    private WebElement element;
    LoginPage passos;

    @Before
    public void init() {
       // driver = Hooks.createChrome();
        passos = new LoginPage();
    }



    @Quando("acesso ao site com login {string} e password {string}")
    public void acessoAoSiteComLoginEPassword(String login, String password) {
        passos.escreverUsuario(login);
        passos.escreverSenha(password);


    }

    @Entao("eu clico no botão Entre")
    public void euClicoNoBotãoEntre() {
    passos.clicarEntre();
    }

    @E("verifico se a pagina inicial foi mostrada")
    public void verificoSeAPaginaInicialFoiMostrada() {
        String logado = passos.validarLogado();
        assertEquals("Próxima",logado,"Não achou a pagina");
        ScreenShot.tirarPrint(driver);
    }
}
