package br.com.inmetrics.teste.GlueCode;

import br.com.inmetrics.teste.pages.FuncionarioPage;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FuncionarioDelete {
    FuncionarioPage passos;

    @Before
    public void init() {
        passos = new FuncionarioPage();
    }


    @Quando("clico no botão deletar")
    public void clico_no_botão_deletar() {
        passos.deletar();
    }

    @Entao("devo validar a mensagem de funcionario deletado com sucesso")
    public void devoValidarAMensagemDeFuncionarioDeletadoComSucesso() {
        String criadoComSucesso = passos.validarFuncionarioCriadocomSucesso();
        assertEquals("SUCESSO! Funcionário removido com sucesso\n" +
                "×", criadoComSucesso, "Valores  não conferem");
        //ScreenShot.tirarPrint(driver);
    }
}




