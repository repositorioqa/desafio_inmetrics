package br.com.inmetrics.teste.GlueCode;

import br.com.inmetrics.teste.pages.FuncionarioPage;
import io.cucumber.java.Before;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FuncionarioEdita {
    FuncionarioPage passos;

    @Before
    public void init() {
        passos = new FuncionarioPage();
    }

    @Quando("eu pesquiso por {string}")
    public void euPesquisoPor(String nome) {
        passos.pesquisar(nome);

    }

    @E("clico no botão editar")
    public void clicoNoBotãoEditar() {
        passos.editar();

    }

    @E("altero os campos nome {string} , cargo {string} e salário {string}")
    public void alteroOsCamposNomeCargoESalário(String nome, String cargo, String salario) {
        passos.escreverNome(nome);
        passos.escreverCargo(cargo);
        passos.escreverSalario(salario);
        passos.clicarEnviar();

    }

    @Entao("devo validar a mensagem de sucesso")
    public void devoValidarAMensagemDeSucesso() {
        String criadoComSucesso = passos.validarFuncionarioCriadocomSucesso();
        assertEquals("SUCESSO! Informações atualizadas com sucesso\n" +
                "×", criadoComSucesso, "Valores  não conferem");
        //ScreenShot.tirarPrint(driver);
    }
}
