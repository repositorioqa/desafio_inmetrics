package br.com.inmetrics.teste.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", tags = "@FuncionarioCadastro",
        glue = {"br/com/inmetrics/teste/GlueCode"},
        dryRun = false,
        snippets = CucumberOptions.SnippetType.CAMELCASE)


public class RunCucumberTest {


}

