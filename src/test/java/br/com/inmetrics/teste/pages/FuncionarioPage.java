package br.com.inmetrics.teste.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class FuncionarioPage {
    private static WebDriver driver = CadastrarFormPage.driver;
    public Select opcoes;

    public void clicarFuncionario() {
        driver.findElement(By.partialLinkText("NOVO")).click();

    }


    public void escreverNome(String nome) {
        driver.findElement(By.name("nome")).sendKeys(nome);
    }

    public void escreverCpf(String cpf) {
        driver.findElement(By.name("cpf")).sendKeys(cpf);
    }

    public void escreverSexo(String sexo) {
        opcoes = new Select(driver.findElement(By.id("slctSexo")));
        opcoes.selectByVisibleText(sexo);


    }

    public void escreverDataDeAdimicao(String data) {

        driver.findElement(By.name("admissao")).sendKeys(data);
    }

    public void escreverCargo(String cargo) {

        driver.findElement(By.name("cargo")).sendKeys(cargo);
    }

    public void escreverSalario(String salario) {
        driver.findElement(By.name("salario")).sendKeys(salario);
    }

    public void tipoDeContratacao() {
        driver.findElement(By.id("clt")).click();

    }

    public void clicarEnviar() {
        driver.findElement(By.className("cadastrar-form-btn")).click();
    }

    public String validarFuncionarioCriadocomSucesso() {
        String criadoComSucesso = driver.findElement(By.cssSelector("div[class='alert alert-success alert-dismissible fade show']")).getText();
        return criadoComSucesso;
    }

    public void pesquisar(String nome) {
        driver.findElement(By.cssSelector("input[type=\"search\"]")).sendKeys(nome);


    }

    public void editar() {
        driver.findElement(By.xpath("//*[@id=\"tabela\"]/tbody/tr[1]/td[6]/a[2]/button")).click();

    }

    public void deletar(){
        driver.findElement(By.id("delete-btn")).click();
    }


}

