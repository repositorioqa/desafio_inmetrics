package br.com.inmetrics.teste.pages;

import br.com.inmetrics.teste.support.Hooks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CadastrarFormPage {
    public static WebDriver driver = Hooks.createChrome();


    public void OpenSite(String site){
        driver.get(site);

    }

    public void clicarCadastre() {
        driver.findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[1]/a")).click();

    }

    public void digitarLogin(String usuario) {
        driver.findElement(By.name("username")).sendKeys(usuario);


    }

    public void digitarSenha(String password) {
        driver.findElement(By.name("pass")).sendKeys(password);


    }

    public void confirmarSenha(String password) {
        driver.findElement(By.name("confirmpass")).sendKeys(password);


    }

    public void clicarCadastrar() {
        driver.findElement(By.className("login100-form-btn")).click();


    }

    public String loginValidacao() {
        String paginaLogin = driver.findElement(By.cssSelector("div form span[class='login100-form-title p-b-1']")).getText();
        return paginaLogin;

    }
}
