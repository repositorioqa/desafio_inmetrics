package br.com.inmetrics.teste.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
   private static WebDriver driver = CadastrarFormPage.driver;



    public void escreverUsuario(String login) {

        driver.findElement(By.name("username")).sendKeys(login);
    }

    public void escreverSenha(String password) {

        driver.findElement(By.name("pass")).sendKeys(password);
    }

    public void clicarEntre() {
        driver.findElement(By.xpath("/html/body/div/div[2]/div/form/div[6]/button")).click();


    }
    public String validarLogado(){
       String logado = driver.findElement(By.linkText("Próxima")).getText();
       return logado;

    }
}
