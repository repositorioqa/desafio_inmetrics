package br.com.inmetrics.teste.support;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.text.SimpleDateFormat;

public class ScreenShot {

    static String dataAtualPasta = new SimpleDateFormat("ddMMyyyy HHmm").format(System.currentTimeMillis());
    static String dataAtualArquivo = new SimpleDateFormat("ddMMyyyy HHmmssSSS").format(System.currentTimeMillis());
    private static String nomeDiretorio = "./evidencias/scenario_" + dataAtualPasta;
    public static void capturar(WebDriver driver,String file){
        File pastaEvidencias = new File(dataAtualPasta);
        File screenshot;

        try{
            if(!pastaEvidencias.exists())
                pastaEvidencias.mkdir();
            screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshot,new File(file));

        } catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
    public static void tirarPrint(WebDriver driver){
    String nomeScreenShot = dataAtualPasta+ "/Imagem_" + dataAtualArquivo + ".png";
    capturar(driver,nomeScreenShot);
    }
}
