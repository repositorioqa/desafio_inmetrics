package br.com.inmetrics.teste.support;

import org.junit.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;


public class Hooks {
    private static WebDriver driver;
    private static final String PATH_CHROMEDRIVER = "./src/test/resources/webdriver/chromedriver.exe";


    public static WebDriver createChrome() {
        System.setProperty("webdriver.chrome.driver", PATH_CHROMEDRIVER);
        driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        return driver;


    }




    @After
    public static void killDriver() {

        if (driver != null) {
            driver.quit();
            driver = null;
        }


    }

}
