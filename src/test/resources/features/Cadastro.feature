#language:pt

  Funcionalidade: Cadastrar um usuario

    @Cadastro
  Cenario: Cadastrar um usuario com sucesso no site

  Dado que eu acesso o site "https://inm-test-app.herokuapp.com/accounts/login/"
    E clicar no botao de cadastro
    Quando eu insiro o usuario "nateusnus" e a senha "123123"
    Entao eu clico no botão cadastrar
      E valido que usuario foi cadastrado com sucesso




