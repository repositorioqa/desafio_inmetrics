#language:pt

Funcionalidade: Editar um funcionario

  @FuncionarioEdita
  Cenario: Editar Funcionario

    Dado que eu acesso o site "https://inm-test-app.herokuapp.com/accounts/login/"
    E acesso ao site com login "mateusnu" e password "123123"
    E eu clico no botão Entre
    Quando eu pesquiso por "teteu"
    E clico no botão editar
    E altero os campos nome "mateus" , cargo "Senior" e salário "35000"
    Entao devo validar a mensagem de sucesso