#language:pt

Funcionalidade: Cadastrar um usuario

  @Logar
  Cenario: Logar com um usuario no site

    Dado que eu acesso o site "https://inm-test-app.herokuapp.com/accounts/login/"
    Quando acesso ao site com login "mateusnu" e password "123123"
    Entao eu clico no botão Entre
    E verifico se a pagina inicial foi mostrada