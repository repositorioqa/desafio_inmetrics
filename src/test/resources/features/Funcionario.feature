#language:pt

Funcionalidade: Cadastrar um funcionario

  @FuncionarioCadastro
  Cenario: Cadastrar um funcionario com sucesso

    Dado que eu acesso o site "https://inm-test-app.herokuapp.com/accounts/login/"
    Quando acesso ao site com login "mateusnu" e password "123123"
    Entao eu clico no botão Entre
    E verifico se a pagina inicial foi mostrada
    Entao eu clico no botão novo funcionario
    Quando exibida insiro Nome "Anderson"
    E Cpf "877.865.400-91"
    E Sexo "Masculino"
    E Data de admisao "01/11/2021"
    E Cargo "Junior"
    E Salario "25122"
    E Tipo de contratação clt
    E Clica em enviar
    E Validar que o funcionario foi criado com sucesso






