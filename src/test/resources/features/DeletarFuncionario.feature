#language:pt

Funcionalidade: Deletar um funcionario

  @FuncionarioDeleta
  Cenario: Deletar Funcionario

    Dado que eu acesso o site "https://inm-test-app.herokuapp.com/accounts/login/"
    E acesso ao site com login "mateusnu" e password "123123"
    E eu clico no botão Entre
    Quando eu pesquiso por "mateus"
    E clico no botão deletar
    Entao devo validar a mensagem de funcionario deletado com sucesso